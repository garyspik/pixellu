import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;
import pageRepo.BasicPage;
import pageRepo.GooglePage;
import pageRepo.PixxelluLandingPage;
import pageRepo.SmartSlidesLoginPage;


public class PixelluTests {

    @Test(priority = 10)
    public void searchAndLogin() throws Exception {
        GooglePage.open("https://www.google.com");
        GooglePage.searchfor("Pixellu");
        GooglePage.log("Search Done");
        GooglePage.textIsVisible("Pixellu - Software for Album Design, Proofing, and Photo Slideshows");
        GooglePage.openSearchResult();
        PixxelluLandingPage.checkCurrentUrl("https://pixellu.com/");
        PixxelluLandingPage.log("Main page visited");
        SmartSlidesLoginPage.wait(1);
        PixxelluLandingPage.opeSmartSlidesLoginPageViaDropDown();
        PixxelluLandingPage.checkCurrentUrl("https://smartslides.com/login");
        SmartSlidesLoginPage.log("Smartslides page visited");
        SmartSlidesLoginPage.loginWithCredentials("pixellu.at.task+5@gmail.com", "cxJ7YP2J");
        SmartSlidesLoginPage.wait(8);
        SmartSlidesLoginPage.textIsVisible("Slideshows");
        SmartSlidesLoginPage.textIsVisible("Increase your slideshow maximum, lift the 30-second limit on downloads, and remove watermarks by upgrading to a subscription");
        SmartSlidesLoginPage.log("Login successful");
    }

    @Test(priority = 20)
    public void LogInWithIncorrectCredentials() throws Exception {
        SmartSlidesLoginPage.open("https://smartslides.com/login");
        SmartSlidesLoginPage.textIsVisible("Log in with your Pixellu Account");
        SmartSlidesLoginPage.log("Smartslides page visited");
        SmartSlidesLoginPage.loginWithCredentials("pixellu.at.task+5@gmail.com", "wongpass1111");
        SmartSlidesLoginPage.log("Incorect credentials provided");
        SmartSlidesLoginPage.wait(8);
        SmartSlidesLoginPage.textIsVisible("Incorrect email or password");
        SmartSlidesLoginPage.checkCurrentUrl("https://smartslides.com/login");
        SmartSlidesLoginPage.log("User cannot login");
    }

    @AfterMethod
    public void afterTest() {
        System.out.println("Test completed");
        BasicPage.clear_cookies();
        System.out.println("Browser cleaned");
    }

    @AfterSuite
    public void afterSuite() {
        BasicPage.driver.quit();
    }
}

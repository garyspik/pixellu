package pageRepo;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

import org.openqa.selenium.io.FileHandler;
import org.testng.Assert;
import org.testng.Reporter;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class BasicPage {

    public static WebDriver driver;
    public static WebDriver setDriver(String browser) {
        switch (browser) {
            case "firefox":
                driver = new FirefoxDriver();
            break;
            case "chrome":
                driver = new ChromeDriver();
            break;
        }
        return  driver;
    }

        public static void open (String url){
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            driver.navigate().to(url);
        }

        public static void wait ( int seconds) throws InterruptedException {
            Thread.currentThread().sleep(seconds * 1000);
        }

        public static void textIsVisible (String text) throws InterruptedException {
            Thread.currentThread().sleep(3000);
            String bodyText = driver.findElement(By.tagName("body")).getText();
            Assert.assertTrue(bodyText.contains(text), "Text not found!");
        }

        public static void clear_cookies () {
            driver.manage().deleteAllCookies();
        }

        public static void log (String input) throws Exception {
            takeScreenshot(input);
            System.out.println(input);
        }

        public static void click (String by){
            driver.findElement(By.xpath(by)).click();
        }

        public static void fillUp (String by, String text){
            driver.findElement(By.xpath(by)).sendKeys(text);
        }

        public static void checkCurrentUrl (String targetUrl){
            Assert.assertEquals(driver.getCurrentUrl(), targetUrl);
        }

        public static void hoverOn (String element){
            WebElement webElement = driver.findElement(By.xpath(element));
            Actions builder = new Actions(driver);
            builder.moveToElement(webElement).perform();
        }

        public static void takeScreenshot (String log) throws Exception {
            String timeStamp;
            File screenShotName;
            File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
            screenShotName = new File("/Users/user/IdeaProjects/Pixellu/src/screenshots" + " " + log + " " + timeStamp + ".png");
            FileHandler.copy(scrFile, screenShotName);
            String filePath = screenShotName.toString();
            String path = "";
            Reporter.log(path);
        }

    }




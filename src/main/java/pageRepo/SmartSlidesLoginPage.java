package pageRepo;

public class SmartSlidesLoginPage extends BasicPage{
     static String loginField = "//input[@name='email']";
    static String passField = "//input[@name='password']";
    static String logInBtn = "//button[@type='submit']";

    public static void loginWithCredentials(String login, String pass) {
        SmartSlidesLoginPage.fillUp(loginField,login);
        SmartSlidesLoginPage.fillUp(passField,pass);
        SmartSlidesLoginPage.click(logInBtn);
    }
}

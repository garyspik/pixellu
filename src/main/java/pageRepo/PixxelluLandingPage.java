package pageRepo;

public class PixxelluLandingPage extends BasicPage {

    static String login = "//li[contains(.,'Login')]";
    static String SmartSlidesLoginBtn = "//a[@title='Log in to SmartSlides']";

    public static void opeSmartSlidesLoginPageViaDropDown() {
        PixxelluLandingPage.hoverOn(login);
        PixxelluLandingPage.click(SmartSlidesLoginBtn);
    }
}

package pageRepo;

public class GooglePage extends BasicPage {
    static String searchSubmit = "//input[@name='btnK']";
    static String searchField = "//input[@name='q']";
    static String pixxellu = "//a[@href='https://pixellu.com/']";

    public static void searchfor(String value) {
        GooglePage.fillUp(searchField, value);
        GooglePage.click(searchSubmit);
    }

    public static void openSearchResult() {
        GooglePage.click(pixxellu);
    }
}

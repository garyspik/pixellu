import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.TestNG;
import pageRepo.BasicPage;


public class Main {


    public static void main(String [] args) throws InterruptedException {
        // browser setup^ use firefox or chrome
        BasicPage.setDriver("firefox");
        //create & run test class as testNG suite
        TestNG testngRunner = new TestNG();
        testngRunner.setTestClasses(new Class[] {
                PixelluTests.class
        });
        testngRunner.run();
    }
}

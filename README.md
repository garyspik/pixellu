1.Download project
2.Open as existing maven project
3.Start selenium server:4444 with java -jar selenium-server-standalone-3.14.0.jar
4.Test files
/Pixellu/src/main/java/PixelluTests.java -test suite
/Pixellu/src/main/java/Main.java - main class to run the tests from
Select from chrome or firefox options in the main class
5.right click run on the main class


Run using command line
1.compile the project using default compiler or run  "mvn compile" "mvn package"
2. go to /Pixellu/out/artifacts/Pixellu_jar/Pixellu.jar or Pixellu/target/Pixellu-1.0-SNAPSHOT.jar
3. Run using "java - jar Pixellu.jar"

Reporting plugin:
Build in TestNG reporting plugin, creates a folder named "test-output" Command line suite/Command line test.html
or make sure testNG is in your class path
java org.testng.TestNG testng.xml


